import React from 'react';
import "../Casscade style-sheet/Components.css"

class Icon extends React.Component {
    render() {
        return (
            <div class="icon-div">
                <img src={this.props.src} alt={this.props.alt} class="icon-img"></img>
                <br></br>
                <label  class="icon-label">{this.props.label}</label>
            </div>
        );
    }
}

export default Icon;