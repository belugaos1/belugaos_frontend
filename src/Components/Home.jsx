import React from 'react';
import "../Casscade style-sheet/Components.css"
import Icon from "./Icon"

import Borrowimg from "../Images/Borrow.svg"
import Docsimg from "../Images/Docs.svg"
import Lendimg from "../Images/Lend.svg"
import Mailimg from "../Images/Mail.svg"
import Profileimg from "../Images/Profile.svg"
import Readmapimg from "../Images/Readmap.svg"
import Referralimg from "../Images/Referral.svg"
import Stakingimg from "../Images/Staking.svg"
import Votingimg from "../Images/Voting.svg"

let images = [
    { object: Borrowimg, name: "Borrow" },
    { object: Docsimg, name: "Docs" },
    { object: Lendimg, name: "Lend" },
    { object: Mailimg, name: "Mail" },
    { object: Profileimg, name: "Profile" },
    { object: Readmapimg, name: "Readmap" },
    { object: Referralimg, name: "Referral" },
    { object: Stakingimg, name: "Staking" },
    { object: Votingimg, name: "Voting" }
]

class Home extends React.Component {
    render() {
        return (
            <div className="images-div">
                {images.map((data) => 
                    <Icon src={data.object} alt={data.label} label={data.label} />
                )}
            </div>
        );
    }
}

export default Home;